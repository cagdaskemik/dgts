# dgts



## Timeline

Start: 01.04.2024 
End: 03.01.2024

Unfortunately, as I had some other personal stuff, I could not commit to my set deadline. I apologize for any planning inconvenience.
I tried to follow the Figma document closely, however it is far from pixel-perfect. This is just a proof-of-concept for demonstrating multiple skills in one project. Many features don't exist and vague requirements are often left as not implemented.

## Project

-Used ContextApi for demonstrating shared state access for posts. Leveraged useContext hook for custom TS PostProvider.
-Used tailwindcss inline styling for faster development.
-Used custom TS Interfaces (types) for type safety (types.ts). Simulated a REST endpoint with mock async API methods (mock.ts). Iteratively mapped items and displayed them on the page.
-Utilized Atomic Design for component Structure. Atoms build up Molecules, and they all come together in Organisms. Templates can hold different organisms, hereby ensuring a modular and scalable approach.
-Added a mock posting method. Utilized React-Router-Dom for SPA routing

## Demo

Try npm i && npm run dev or test it live on http://dgts.vercel.app